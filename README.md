# Workstation maker

The target of this project is to setup a SD Card for a raspberry with additional
features as WIFI, SSH, crontabs to avoid plugging any hardware for the first boot.

## Getting Started

This is POSIX-compliant shell script with all dependancies available in
busybox, so it should be working on nearly all linux setups.

### Prerequisites

Download a [raspbian image here](https://www.raspberrypi.org/downloads/raspbian/).

### Usage
```
Usage: workstation-setup.sh -d SD_CARD [OPTIONS]

Setup an SD card for a raspberry
    -c  'CRONTAB_ENTRY' to add to pi crontab, can either be a file or the content
    -d  'SD_CARD' to format
    -D  Dry run
    -i  'UPDATE_FILE' to use for flashing
    -h  Print this help text and exit
    -H  'HOSTNAME' to set
    -w  'WIFI' configuration to inject, can either be the file or the content
    -x  'EXEC:LOC' to inject an executable inside the image at LOC
        This option can be passed multiple times to add multiples entries

SSH options
    -a  'AUTHORIZED_KEYS' to accept, disable the password authentication when provided
        Can either be the file or the content
    -s  enable ssh service
    -p  'PUBLIC_KEY' to inject
    -k  'PRIVATE_KEY' to inject
    -K  'KNOWN_HOSTS' to inject, can either be the file or the content
    -P  'PROFILE' to inject in .ssh/profile, can either be the file or the content
All options listed between [brackets] can also be passed in environment variables.
```

The only mandatory option is the SD_CARD path, like this you can just enable ssh with:
```
workstation-setup.sh -d SD_CARD -s
```
But you can still build a much more complex setup:
```
workstation-setup.sh -a "$(cat ~/.ssh/id_rsa.pub)" \
                      -c "* * * * * /usr/bin/my_script" \
                      -d "/dev/mmcblk0" \
                      -i "2020-02-13-raspbian-buster-lite.img" \
                      -H "myraspberry" \
                      -s \
                      -x "my_script:/usr/bin/my_script" \
                      -x "my_script_2:/usr/bin/my_script_2"
```

## Limitations

- This was only tested on a subset of Raspberry pi and Raspbian
- No warning when erasing the sd card
- CTRL-C is not correctly handled

